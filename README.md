# s4-multitask

This repository contains code for training, testing, and visualisation of S4 models as presented in [Structured State Space Models for Multiple Instance Learning in Digital Pathology](https://arxiv.org/abs/2306.15789). The repo is based on the [TransMIL repository](https://github.com/szc19990412/TransMIL/tree/main), with the `PyTorch Lightning` code ported to plain `PyTorch` and with various extensions included. By default, model training performs multiple instance learning, and, where available utilises tile-level annotations to perform multitask training and inference.

The repository relies on a tiling and feature extraction pipeline based on [CLAM](https://github.com/mahmoodlab/CLAM). The specifics of feature extraction are contained in `scripts/features.sh`, but this still relies on the initial tissue segmentation and tiling to be performed by CLAM or other, which is out of scope for this repository.

To understand the required data formats, refer to:

* **/workdir/shared/cpm4c/MIL_Features** - the whole slide images, which should be contained in a single-level directory (symbolic links can be used)

* **/workdir/shared/cpm4c/MIL_Patches** - the tiling and quality control outputs (e.g. segmentation masks)

* **/workdir/shared/cpm4c/MIL_Features** - the ResNet50 features extracted for each tile

As of *31/08/23*, the above data has been preprocessed for the datasets studied in *Structured State Space Models for Multiple Instance Learning in Digital Pathology*, as well as the following Gustave Roussy collaborations:

* **BCLN** - breast cancer lymph node dataset
* **Prostate** - prostate cancer dataset (the public PANDA dataset, also)
* **CD8** - lung cancer CD8 dataset

N.B. on occasion, a small number of slides are excluded from the analysis due to quality control. Generally, this is indicated in a file called `REMOVED.txt` in the corresponding MIL_Patches or MIL_Features subdirectory.

## Experimental setup

In most cases, training and testing follows a *nested cross-validation* (NCV). That is, training and testing proceeds according to a series of scheduled folds (usually 5 or 10), with each case (patient) featuring in exactly one test fold. For each iteration of NCV, the training data is further split 80-20 (patient-wise) into training and validation data, which drives model checkpointing. After each iteration of NCV, the checkpointed best-performing model on the iteration's validation fold is evaluated on the test fold. The aggregated performance across all test folds gives the estimated generalisation performance of the model pipeline. The checkpoints are preserved in the `saved_models` directory for further use.

## Installation

```
$ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/workdir/shared/cpm4c/openslidelib
$ export LD_LIBRARY_PATH
$ module load anaconda3/2022.10/gcc-11.2.0
$ pip install -r requirements.txt
```

## Monitoring

The train and evaluation code logs outputs to `weights and biases` (wandb). It is required to create an account and initiate the web hook with,

```
$ WANDB__SERVICE_WAIT=300 wandb login
```

and to follow the authentication instructions.

## Reproducibility

To reproduce results, simply run the desired experiment with the `train.sh` script, using the `--stage test` argument. This evalutes the pretrained models in `saved_models`. N.B. in the GR prostate experiments, the best performing model is simply the baseline, and no S4D weights have been included. Results can therefore be reproduced using the `create_splits.py` code (see below), and inspecting the output file.

## Experiment configuration

Each experiment is defined by a separate configuration file. See `configs/README.md` for details on configuration parameters. Given a correctly defined configuration file, an experiment can be run with the same set of modules.. The available modules are detailed below:

### Create splits

The `create_splits.py` module, called on the Mesocentre with `sbatch create_splits.sh`, contains code for exporting the NCV folds. It also runs a baseline max pooling model on each split as it is created.The number of splits is specified by the `--n_splits` argument. The NCV folds are exported as `.csv` files to the `splits` directory. When a designated test set is required (for example, when incorporating an external dataset), this should be created manually rather than using this module.

### Training and evaluation

The `train.py` module, called on Mesocentre with `sbatch train.sh`, contains code for both training and evaluation of models. By default, both training and testing are performed. Testing can be performed on previously checkpointed models simply by setting the `--stage test` argument. In each case, all folds of NCV are performed.

### Inference

The `infer.py` module, called on Mesocentre with `sbatch test.sh`, contains code for applying trained models on whole slide images and creating visualisations (comparing with ground truth annotations where available). It should be called with the same configuration file as used for training. It also follows the folds of NCV, such that no trained model infers on WSIs seen in training. In future this module could be absorbed into the testing code, although it would add significantly to the runtime.

### Fine-tuning

A model can be pretrained on one dataset and finetuned on another by setting the `pretrained_model` in the configuration file. This will be loaded prior to the start of training.

### Evaluation with pretrained model

As the evaluation code anticipates a fixed naming convention, the easiest way to evaluate a pretrained model (without fine-tuning) is to copy the model file to the appropriate `saved_models` subdirectory, according to the convention `fold0.pt, fold1.pt, ...`. These will be then be loaded by the evaluation code.

## Workflow

Although this repository is intended for analysis only, some guidance is hereby given on the preprocessing stages. There are two such stages: tiling, where ; and feature extraction. Although a tool like CLAM is equipped to carry out both stages, CLAM was used only for the tiling stage, with a custom script (see below) written for the feature extraction.

### Tiling

The create_patches_fp.py script of CLAM was used for the tiling procedure. This is called with `scripts/tiling.sh`, but must be set up separately. The script outputs offer important quality control check, also.

### Feature extraction

All ResNet50 feature extraction was performed according to `scripts/extract_features.py`, although could have been performed with CLAM scripts. It is important to note the precise set of pretrained weights used, as more than one set is available through `PyTorch`.
