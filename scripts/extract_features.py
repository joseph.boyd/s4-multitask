import os
import h5py
import glob
import tqdm
import argparse

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models
#from openslide import OpenSlide
#import openslide
import tiffslide as openslide

from utils import write_flush


class ResnetFeatures(nn.Module):

    def __init__(self):

        super(ResnetFeatures, self).__init__()

        resnet = models.resnet50(weights='IMAGENET1K_V2')

        self.features = nn.Sequential(
            resnet.conv1,
            resnet.bn1,
            resnet.relu,
            resnet.maxpool,
            resnet.layer1,
            resnet.layer2,
            resnet.layer3)
            #resnet.layer4)  # in CLAM, the final conv block is omitted

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))

        self.register_buffer('means', torch.tensor([0.485, 0.456, 0.406]).view(3, 1, 1))
        self.register_buffer('stds', torch.tensor([0.229, 0.224, 0.225]).view(3, 1, 1))

    def forward(self, x):

        x = (x - self.means) / self.stds  # normalise as per imagenet inputs

        x = self.features(x)
        x = self.avgpool(x)
        x = torch.flatten(x, 1)

        return x

def main(task_id, n_tasks, dir_coords, dir_slides, dir_feats, tile_size, batch_size=32):

    write_flush(dir_feats)

    os.makedirs(dir_feats, exist_ok=True)
    os.makedirs(os.path.join(dir_feats, 'pt_files'), exist_ok=True)
    os.makedirs(os.path.join(dir_feats, 'h5_files'), exist_ok=True)

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    write_flush(device)

    net = ResnetFeatures().to(device).eval()

    coords_files = sorted(os.listdir(dir_coords))

    for coords_file in coords_files[task_id::n_tasks]:

        if os.path.exists(os.path.join(dir_feats, 'h5_files', coords_file)):
            continue

        write_flush(f'Processing {coords_file}...')

        f = h5py.File(os.path.join(dir_coords, coords_file), 'r')
        coords = f['coords'][()]

        slide_path = glob.glob(os.path.join(dir_slides, os.path.splitext(coords_file)[0] + '*'))[0]
        wsi = openslide.OpenSlide(slide_path)

        write_flush('Reading tiles...')

        tiles = [wsi.read_region((x, y), level=0, size=(tile_size, tile_size)) for x, y in coords]
        f.close()

        tiles = np.stack(tiles)

        N = len(tiles)
        features = torch.empty((N, 1024))

        write_flush('Processing batches...')

        for batch_i in tqdm.tqdm(range(0, N, batch_size)):

            # preprocess batch
            x_batch = torch.Tensor(tiles[batch_i:min(N, batch_i + batch_size)]) / 255.
            x_batch = x_batch.permute((0, 3, 1, 2))[:, :3]
            x_batch = F.interpolate(x_batch, size=256, mode='bilinear', align_corners=True)

            with torch.no_grad():
                net_outputs = net(x_batch.to(device))
                features[batch_i:min(N, batch_i + batch_size)] = net_outputs.detach().cpu()

        # save .h5 file
        f = h5py.File(os.path.join(dir_feats, 'h5_files', coords_file), 'w')
        dset = f.create_dataset('coords', data=coords)
        dset = f.create_dataset('features', data=features.numpy())
        f.close()

        # save .pt file
        torch.save(features, os.path.join(dir_feats, 'pt_files', coords_file))

        write_flush('Done.')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Feature extraction')
    parser.add_argument('--task_id', type=int, default=None, help='task id to divide work inside job array')
    parser.add_argument('--n_tasks', type=int, default=None, help='number of tasks in job array')
    parser.add_argument('--patches_dir', type=str, default=None, help='path to patch coordinates')
    parser.add_argument('--wsi_dir', type=str, default=None, help='path to WSIs')
    parser.add_argument('--output_dir', type=str, default=None, help='output path')
    parser.add_argument('--tile_size', type=int, default=None)
    args = parser.parse_args()
    write_flush(args.task_id)

    main(args.task_id, args.n_tasks, args.patches_dir, args.wsi_dir, args.output_dir, args.tile_size)

