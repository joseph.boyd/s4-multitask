#!/bin/bash
#SBATCH --job-name=s4-features
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=mem
#SBATCH --mem=180000

# Activate anaconda environment code
source activate $HOME/miniconda3/envs/clam

python extract_features.py --task_id $SLURM_ARRAY_TASK_ID --n_tasks $SLURM_ARRAY_TASK_COUNT --patches_dir /workdir/shared/cpm4c/MIL_Patches/panda/patches --wsi_dir /workdir/shared/cpm4c/prostate-cancer-grade-assessment/ --output_dir /workdir/shared/cpm4c/MIL_Features/panda/20x --tile_size 256

