# scripts

Feature extraction is configured to be submitted as a job array, to mitigate mesocentre's abysmal I/O latency:

`sbatch --array=0-29 -J array features.sh`

See [https://gitlab-research.centralesupelec.fr/joseph.boyd/olympus-convert](https://gitlab-research.centralesupelec.fr/joseph.boyd/olympus-convert) for instructions of how to convert files from Olympus microscope format.

