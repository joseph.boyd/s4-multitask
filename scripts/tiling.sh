#!/bin/bash
#SBATCH --job-name=s4-tiling
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --partition=gpua100
#SBATCH --mem=180000

# Activate anaconda environment code
source activate $HOME/miniconda3/envs/clam
cd CLAM

python create_patches_fp.py --source /workdir/shared/cpm4c/CD8_TOPUP --save_dir /workdir/shared/cpm4c/MIL_Patches/CD8_TOPUP/40x --patch_size 256 --step_size 256 --seg --patch --stitch --process_list process_list.csv #--no_auto_skip

