#!/bin/bash
#SBATCH --job-name=s4-baseline
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=gpua100
#SBATCH --gres=gpu:1
#SBATCH --mem=180000

# Activate anaconda environment code
source activate $HOME/miniconda3/envs/clam

python create_splits.py --exp_id prostate/prostate_gleason \
                        --dir_features /workdir/shared/cpm4c/MIL_Features/prostate/20x/h5_files \
                        --metadata /workdir/shared/cpm4c/MIL_Features/prostate/metadata_gleason.csv \
                        --label label \
                        --n_splits 5

python create_splits.py --exp_id prostate/prostate_binary \
                        --dir_features /workdir/shared/cpm4c/MIL_Features/prostate/20x/h5_files \
                        --metadata /workdir/shared/cpm4c/MIL_Features/prostate/metadata_binary.csv \
                        --label label \
                        --n_splits 5
exit 0

python create_splits.py --exp_id prostate/panda \
                        --dir_features /workdir/shared/cpm4c/MIL_Features/panda/20x/h5_files \
                        --metadata /workdir/shared/cpm4c/MIL_Features/panda/metadata_panda.csv \
                        --label label \
                        --n_splits 1
exit 0

python create_splits.py --exp_id cd8/cd8_topup \
                        --dir_features /workdir/shared/cpm4c/MIL_Features/CD8_TOPUP/40x/h5_files \
                        --metadata /workdir/shared/cpm4c/MIL_Features/CD8_TOPUP/metadata_topup.csv \
                        --label label \
                        --n_splits 5
exit 0

python create_splits.py --exp_id cd8/cd8_all \
                        --dir_features /workdir/shared/cpm4c/MIL_Features/CD8_TOPUP/40x/h5_files \
                        --metadata /workdir/shared/cpm4c/MIL_Features/CD8_TOPUP/metadata.csv \
                        --label label \
                        --n_splits 5

exit 0

python create_splits.py --exp_id cd8/cd8 \
                        --dir_features /workdir/shared/cpm4c/MIL_Features/CD8/40x/h5_files \
                        --metadata /workdir/shared/cpm4c/MIL_Features/CD8/metadata.csv \
                        --label label \
                        --n_splits 5
exit 0
