import os
import json
import glob
import argparse
import h5py
import numpy as np
import pandas as pd
import torch

import cv2
import matplotlib.pyplot as plt
from matplotlib import cm
from skimage.io import imsave
from skimage.transform import resize
#import openslide
import tiffslide as openslide
from PIL import Image, ImageDraw
from torchvision.utils import make_grid
from skimage.io import imread, imsave

from src.models import S4Former
from src.utils import *


device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
write_flush(str(device))

#--->Setting parameters
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str)
    parser.add_argument('--annot_dir', type=str)
    parser.add_argument('--wsi_dir', type=str)
    parser.add_argument('--folds', type=int)
    args = parser.parse_args()
    return args

#--->Utility functions
def get_grid(wsi, coords):
    idx = np.random.choice(coords.shape[0], size=8)
    coords = coords[idx]
    tiles = np.stack([
        np.array(wsi.read_region((c[0], c[1]), level=0, size=(512, 512)))[..., :3]
        for c in coords[:8]])

    tiles = torch.Tensor(tiles).permute((0, 3, 1, 2))
    grid = make_grid(tiles, nrow=4).permute((1, 2, 0))
    return grid

def add_text(img, text):
    font = cv2.FONT_ITALIC
    return cv2.putText(img, text, (10, 30), font, 1, (0, 0, 0), 2)

def img_compose(wsi, attention_img, mask_img=None, cmap='jet', opacity=0.5, vis_level=-6):

    vis_level = min(len(wsi.level_dimensions) - 1, vis_level)
    img_size = wsi.level_dimensions[vis_level]
    # Applies jet colormal on the attention map
    heatmap = 255 * cm.get_cmap(plt.get_cmap(cmap))(attention_img)
    # Applies a 0.5 opacity to the heatmap color 
    heatmap[:, :, 3] *= opacity # This is performing a masking to the heatmap 

    heatmap[heatmap[..., 0] == 0] = 0

    # This gets the image thumbnail
    sld = wsi.get_thumbnail((img_size[0], img_size[1])).convert('RGBA')
    heatmap = resize(heatmap, output_shape=np.array(sld).shape, order=3)
    # This creates the overlay using the heatmap
    gcm = Image.fromarray(np.uint8(heatmap),'RGBA')

    # sld = sld.crop((3300, 1000, 3800, 1500))
    # gcm = gcm.crop((3300, 1000, 3800, 1500))

    return Image.alpha_composite(sld, gcm)

def get_annotation_mask(annotation_path, width, height, downsample=100):

    mask = Image.new('L', (int(width / downsample), int(height / downsample)), 0)

    if os.path.isfile(annotation_path):

        annotations = json.load(open(annotation_path))
    
        for annot_set in annotations:
            for annot in annot_set['geometry']['coordinates']:
                polygon = np.array(annot) / downsample
                coords = list(zip(polygon[:, 0], polygon[:, 1]))
                ImageDraw.Draw(mask).polygon(coords, outline=255, fill=255)

    return np.array(mask)

def export_high_and_low(wsi, tile_probs, coords, save_path):
    hi_probs = coords[tile_probs[0, :, 1] > 0]# > 0.95]
    lo_probs = coords[tile_probs[0, :, 1] > 0]# < 0.05]

    hi_grid = get_grid(wsi, hi_probs)
    lo_grid = get_grid(wsi, lo_probs)

    imsave(save_path, torch.cat([lo_grid, hi_grid]))

def main(args, cfg, fold):

    write_flush(f'Processing fold {fold}')

    n_classes = cfg.Data.n_classes
    d_model, d_state = cfg.Model.d_model, cfg.Model.d_state
    costs = cfg.Model.costs

    log_path = cfg.General.log_path
    model_path = f'{log_path}/fold{fold}.pt'

    model = S4Former(n_classes, 1024, d_model, d_state, costs).to(device)
    model.load_state_dict(torch.load(model_path))
    model.eval()

    df_fold = pd.read_csv(f'{cfg.Data.label_dir}/fold{fold}.csv')
    df_test_slides = df_fold[['test', 'test_label']].dropna()

    class_names = cfg.Data.class_names

    os.makedirs('./heatmaps', exist_ok=True)

    for i, row in df_test_slides.iterrows():

        if os.path.isfile(f'heatmaps/{row.test}_heatmap.png'):
            write_flush(f'Skipping heatmaps/{row.test}')
            continue

        h5_file = row.test + '.h5'
        write_flush(f'Processing {h5_file}...')

        with h5py.File(os.path.join(cfg.Data.data_dir, h5_file), 'r') as f:
            features = f['features'][()]
            coords = f['coords'][()]
    
        output_dict = model(data=torch.Tensor(features[None, ...]).to(device))
        tile_probs = output_dict['Y_prob_tile'].detach().cpu()
    
        y_pred = output_dict['Y_hat'].item()
        y_prob = output_dict['Y_prob'][0, y_pred].item()
    
        pred_class = class_names[y_pred]
        try:
            slide_name = glob.glob(f'{args.wsi_dir}/{h5_file.split(".")[0]}*')[0]
        except:
            continue
        wsi = openslide.OpenSlide(slide_name)

        export_high_and_low(wsi, tile_probs, coords, f'heatmaps/{row.test}_samples.png')

        W, H = wsi.dimensions

        # derive annotation
        mask = get_annotation_mask(f'{args.annot_dir}/{row.test}.json', W, H)
    
        # derive heatmap
        downsample = 512
        h, w = H // downsample, W // downsample
        heatmap = np.zeros((h, w))

        for i, (x, y) in enumerate(coords):
            heatmap[min(h - 1, y // downsample),
                    min(w - 1, x // downsample)] = tile_probs[0, i, 1]
    
        heatmap[heatmap < 0.5] = 0
    
        # construct output
        im_mask = np.array(img_compose(wsi, mask, vis_level=5))
        class_name = class_names[int(row.test_label)]
        im_mask = add_text(im_mask, f'Ground truth [{class_name}]')
    
        im_heatmap = np.array(img_compose(wsi, heatmap, vis_level=5))
        im_heatmap = add_text(im_heatmap, 'Prediction [%s | %.04f]' % (pred_class, y_prob))
      
        mosaic = np.vstack([
            np.pad(im_mask[..., :3], pad_width=[(0, 10), (0, 0), (0, 0)]),
            im_heatmap[..., :3]]
        )
    
        imsave(f'heatmaps/{row.test}_heatmap.png', mosaic)

if __name__ == '__main__':

    args = parse_args()
    cfg = read_yaml(args.config)

    write_flush(args)

    for fold in range(args.folds):
        main(args, cfg, fold)

