import os
import argparse
import pandas as pd
import numpy as np

from sklearn.model_selection import KFold, train_test_split
from sklearn.metrics import confusion_matrix, classification_report

from src.models import mean_pooling, max_pooling
from src.utils import *


#--->Setting parameters
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--exp_id', type=str)
    parser.add_argument('--dir_features', type=str)
    parser.add_argument('--metadata', type=str)
    parser.add_argument('--label', type=str, default='label')
    parser.add_argument('--n_splits', type=int)

    args = parser.parse_args()
    return args

def export_split(exp_id, df_train, df_test, label, k):

    split_dir = f'./splits/{exp_id}'
    os.makedirs(split_dir, exist_ok=True)

    tr_cases = df_train.case_id.unique()
    tr_cases, val_cases = train_test_split(tr_cases, test_size=0.3)

    df_val = df_train[df_train.case_id.isin(val_cases)]
    df_train = df_train[df_train.case_id.isin(tr_cases)]

    len_train, len_val, len_test = df_train.shape[0], df_val.shape[0], df_test.shape[0]
    max_split = max(len_train, len_val, len_test)

    df_splits = pd.DataFrame({
        'train' : max_split * [''],
        'val' : max_split * [''],
        'test' : max_split * [''],
        'train_label' : max_split * [''],
        'val_label' : max_split * [''],
        'test_label' : max_split * ['']})

    df_splits.iloc[:len_train, 0] = df_train['slide_id']
    df_splits.iloc[:len_val, 1] = df_val['slide_id']
    df_splits.iloc[:len_test, 2] = df_test['slide_id']
    df_splits.iloc[:len_train, 3] = df_train[label]
    df_splits.iloc[:len_val, 4] = df_val[label]
    df_splits.iloc[:len_test, 5] = df_test[label]

    df_splits.to_csv(os.path.join(split_dir, 'fold%d.csv' % k))

def create_splits(feat_dict, df_metadata, n_splits=10):

    # N.B. sorted(list(set())) ensures slides are split deterministically
    class_labels = sorted(list(set(df_metadata[args.label])))
    nb_classes = len(class_labels)

    unique_cases = sorted(list(set(df_metadata.case_id)))
    nb_cases = len(unique_cases)

    if n_splits == 1:
        kfold = train_test_split(range(nb_cases), test_size=0.1, shuffle=True, random_state=0)
        split_data = [(0, kfold)]
    else:
        kfold = KFold(n_splits=n_splits, shuffle=True, random_state=0)
        split_data = enumerate(kfold.split(unique_cases))

    nb_seqs = len(feat_dict)
    write_flush(f'Sequences: {nb_seqs}')

    seq_lens = [len(seq) for seq in feat_dict.values()]
    min_len, mean_len, max_len = min(seq_lens), sum(seq_lens) // nb_seqs, max(seq_lens)

    write_flush(f'Min len: {min_len}, mean len: {mean_len}, max len: {max_len}')

    d_input = feat_dict[list(feat_dict)[0]].shape[1]
    write_flush(f'Input dimension: {d_input}')

    # Patients with >1 WSI must not be split across folds!
    for k, (idx_train_cases, idx_test_cases) in split_data:

        write_flush(f'[k = {k}]')

        train_cases = np.array(unique_cases)[idx_train_cases]
        test_cases = np.array(unique_cases)[idx_test_cases]

        df_train = df_metadata[df_metadata.case_id.isin(train_cases)]
        df_test = df_metadata[df_metadata.case_id.isin(test_cases)]

        df_train = df_train[df_train['slide_id'].isin(feat_dict.keys())].sample(frac=1)  # shuffle rows
        df_test = df_test[df_test['slide_id'].isin(feat_dict.keys())]

        export_split(args.exp_id, df_train, df_test, args.label, k)

        x_train = [feat_dict[slide_id] for slide_id in df_train.slide_id]
        x_test = [feat_dict[slide_id] for slide_id in df_test.slide_id]

        y_train = [class_labels.index(label) for label in df_train[args.label]]
        y_test = [class_labels.index(label) for label in df_test[args.label]]

        model = max_pooling()
        model.fit(x_train, y_train)

        preds, probs = model.predict(x_train)
        evals = evaluate(y_train, preds)

        write_flush('[Train]\t' + '\t'.join('%s: %.02f' % (key, evals[key]) for key in evals))

        preds, probs = model.predict(x_test)
        evals = evaluate(y_test, preds)

        write_flush('[Test]\t' + '\t'.join('%s: %.02f' % (key, evals[key]) for key in evals))

        cr = classification_report(y_test, preds, output_dict=True)

        write_flush(cr)

        write_flush('Slide confusion:')
        cm = confusion_matrix(y_test, preds, labels=list(range(nb_classes)))
        write_flush(cm)


if __name__ == '__main__':

    args = parse_args()

    feature_dict = get_features(args.dir_features)

    df_metadata = pd.read_csv(args.metadata)[['slide_id', 'case_id', args.label]]
    create_splits(feature_dict, df_metadata, args.n_splits)

