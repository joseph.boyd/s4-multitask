import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from . import s4

from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler


class Pooling:

    def __init__(self, pool, *args, **kwargs):

        self.scaler = StandardScaler()
        self.pool = pool
#        self.clf = RandomForestClassifier(class_weight='balanced',
#                                          n_estimators=200,
#                                          max_depth=3,
#                                          random_state=0)
        self.clf = LogisticRegression(class_weight='balanced', random_state=0, C=1)

    def fit(self, x_train, y_train, *args, **kwargs):

        x_train = torch.concat([self.pool(x).unsqueeze(0) for x in x_train])
        self.scaler.fit(x_train)
        x_train = self.scaler.transform(x_train)
        self.clf.fit(x_train, y_train)

    def predict(self, x_test, *args, **kwargs):

        x_test = torch.concat([self.pool(x).unsqueeze(0) for x in x_test])
        x_test = self.scaler.transform(x_test)
        preds = self.clf.predict(x_test)
        probs = self.clf.predict_proba(x_test)

        return preds, probs


def mean_pooling(*args, **kwargs):
    pool_func = lambda x : torch.mean(x, axis=0)
    return Pooling(pool_func)


def max_pooling(*args, **kwargs):
    pool_func = lambda x : torch.max(x, axis=0)[0]
    return Pooling(pool_func)


class TileLoss(nn.Module):

    def __init__(self, weights):

        super(TileLoss, self).__init__()
        self.register_buffer('weights', weights)
        self.register_buffer('dummy', torch.Tensor([0]))

    def forward(self, logits_tile, annotation):

        if annotation.shape[1] == 0:
            return self.dummy

#        counts = torch.bincount(annotation[0], minlength=2)
#        weights = (1 / 2) * (1 / counts)
#        weights /= weights.sum()
#        weights += 1e-5

        criterion = nn.CrossEntropyLoss(weight=self.weights)
        tile_loss = criterion(logits_tile, annotation)

        return tile_loss


class S4Former(nn.Module):

    def __init__(self, n_classes, d_input, d_model, d_state, costs=None):

        super(S4Former, self).__init__()

        self.n_classes = n_classes

        self.fc1 = nn.Sequential(
                        nn.Linear(d_input, d_model),
                        nn.ReLU(),
                        nn.LayerNorm(d_model))

        self.ss1 = s4.S4D(d_model=d_model, d_state=d_state, transposed=False)

        self.fc_tiles = nn.Sequential(
                        nn.Linear(d_model, 2))

        self.fc_slide = nn.Sequential(
                        nn.Linear(d_model, self.n_classes))

        if costs is None:  # uniform default costs
            costs = torch.ones(n_classes, n_classes) - torch.eye(n_classes)

        self.register_buffer('costs', torch.Tensor(costs))

    def set_costs(self, costs):

        self.costs = costs

    def cost_sensitive_prediction(self, probs):

        probs = torch.matmul(probs, self.costs.T)
        preds = torch.argmin(probs, axis=1)

        return preds

    def forward(self, **kwargs):
        x = kwargs['data'].float()

        x = self.fc1(x)
        x = self.ss1(x)

        # tile-level head
        logits_tile = self.fc_tiles(x)
        Y_prob_tile = F.softmax(logits_tile, dim=2)
        Y_hat_tile = torch.argmax(Y_prob_tile, dim=2)

        # max pool along sequence
        x = torch.max(x, axis=1).values

        # slide-level head
        logits_slide = self.fc_slide(x)
        Y_prob_slide = F.softmax(logits_slide, dim=1)
        Y_hat_slide = self.cost_sensitive_prediction(Y_prob_slide)

        results_dict = {
            'logits': logits_slide,
            'Y_prob': Y_prob_slide,
            'Y_hat': Y_hat_slide,
            'logits_tile' : logits_tile,
            'Y_prob_tile' : Y_prob_tile,
            'Y_hat_tile' : Y_hat_tile}

        return results_dict

