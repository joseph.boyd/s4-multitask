import warnings
warnings.simplefilter(action='once')

import os
import h5py
import random
import torch
import numpy as np
import pandas as pd

import torch.utils.data as data
from torch.utils.data import dataloader

from src.utils import write_flush


class MILDataset(data.Dataset):
    def __init__(self, dataset_cfg=None, state=None):

        # Set all input args as attributes
        self.__dict__.update(locals())
        self.dataset_cfg = dataset_cfg

        #---->data and label
        self.nfolds = self.dataset_cfg.nfold
        self.fold = self.dataset_cfg.fold
        self.feature_dir = self.dataset_cfg.data_dir
        self.csv_dir = os.path.join(self.dataset_cfg.label_dir, f'fold{self.fold}.csv')
        self.annot_dir = self.dataset_cfg.annot_dir
        self.slide_data = pd.read_csv(self.csv_dir, index_col=0)
        self.n_classes = dataset_cfg.n_classes
        self.class_names = dataset_cfg.class_names

        self.state = state

        #---->split dataset
        if state == 'train':
            self.data = self.slide_data.loc[:, 'train'].dropna()
            self.labels = self.slide_data.loc[:, 'train_label'].dropna()
        if state == 'val':
            self.data = self.slide_data.loc[:, 'val'].dropna()
            self.labels = self.slide_data.loc[:, 'val_label'].dropna()
        if state == 'test':
            self.data = self.slide_data.loc[:, 'test'].dropna()
            self.labels = self.slide_data.loc[:, 'test_label'].dropna()

#        self.label_encoding = dict(zip(self.class_names, range(self.n_classes)))
#        self.labels = pd.Series([self.label_encoding[label] for label in self.labels])

        #---->read data
        self.features, self.coords, self.annotations = [], [], []

        for slide_id in self.data:

            feature_path = os.path.join(self.feature_dir,  f'{slide_id}.h5')

            with h5py.File(feature_path, 'r') as f:
                features = torch.Tensor(f['features'][()])
                coords = f['coords'][()]

            self.features.append(features)
            self.coords.append(pd.DataFrame(data=coords))

            annotation_path = os.path.join(self.annot_dir,  f'{slide_id}.h5')

            if not os.path.exists(annotation_path):
                annotation = -torch.ones(features.shape[0])
            else:
                with h5py.File(annotation_path, 'r') as f:
                    annotation = torch.Tensor(f['labels'][()])

            self.annotations.append(annotation)

        self.summary()

    def summary(self):

        write_flush(self.csv_dir, self.state)
        seq_lens = [feats.shape[0] for feats in self.features]
        num_seqs = len(seq_lens)
        write_flush(f'{self.state.capitalize()} dataset: {num_seqs} samples.')
        write_flush(self.labels.value_counts().sort_index())
        write_flush('Min len: %d Mean len: %.0f Max len: %d' % (min(seq_lens),
                                                                sum(seq_lens) / num_seqs,
                                                                max(seq_lens)))

    def __len__(self):

        return len(self.data)

    def __getitem__(self, idx):

        if self.state == 'train':
            indices = np.arange(self.labels.shape[0])
            counts = np.bincount(self.labels, minlength=self.n_classes)
            proportions = (1 / self.n_classes) * (1 / counts)
            probabilities = [proportions[label] for label in self.labels]
            idx = np.random.choice(indices, 1, p=probabilities)[0]

        slide_id = self.data[idx]
        features = self.features[idx] 
        annotation = self.annotations[idx] 
        label = int(self.labels[idx])

        # random rotation of slide
        df_coords = self.coords[idx]
        df_coords = (df_coords / 512).round().astype('int')
        df_coords = df_coords - df_coords.min()

        len_seq = features.shape[0]

        # cap outlier length - random subset
        if len_seq > 25000:
            cap_idx = torch.randperm(len_seq)
            cap_idx = cap_idx[:25000]
            cap_idx = torch.sort(cap_idx)[0]

            features = features[cap_idx]
            annotation = annotation[cap_idx]
            df_coords = df_coords.iloc[cap_idx].reset_index(drop=True)
            len_seq = features.shape[0]

        # augmentation
        if self.state == 'train':

            if np.random.rand() > 0.5:
                df_coords.sort_values(by=[0, 1])
            else:
                df_coords.sort_values(by=[1, 0])

            if np.random.rand() > 0.5:
                df_coords = df_coords.iloc[::-1]

            sort_idx = df_coords.index.values

            features = features[sort_idx]
            annotation = annotation[sort_idx]
            df_coords = df_coords.reset_index(drop=True)

            # random rotation of flattened sequence
            shift = torch.randint(0, len_seq, size=(1,)).item()
            features = features.roll(shifts=shift, dims=0)
            annotation = annotation.roll(shifts=shift, dims=0)
            rolled_coords = np.roll(df_coords[df_coords.columns].values, shift=shift, axis=0)
            df_coords = pd.DataFrame(data=rolled_coords)

            # random removal
            mask = torch.rand(len_seq) < 0.95
            features = features[mask]
            annotation = annotation[mask]
            df_coords = df_coords[mask.numpy()]

        coords = torch.Tensor(df_coords.values.copy()).long()

        return features, label, annotation, slide_id, coords

