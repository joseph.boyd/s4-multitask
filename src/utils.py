import os
import sys
import glob
import h5py
import yaml
import random
from functools import reduce
from addict import Dict
import numpy as np
import pandas as pd
from toolz.dicttoolz import valmap

import wandb
import torch
import torch.nn as nn
from sklearn.metrics import accuracy_score, roc_auc_score, balanced_accuracy_score, f1_score
from sklearn.metrics import confusion_matrix, precision_score, recall_score, classification_report


def f1_catch(*args, **kwargs):
    try:
        return f1_score(*args, **kwargs)
    except:
        return 0

def roc_auc_catch(*args, **kwargs):
    try:
        return roc_auc_score(*args, **kwargs)
    except:
        return 0

def write_flush(*text_args, stream=sys.stdout):
    stream.write(', '.join(map(str, text_args)) + '\n')
    stream.flush()
    return

def count_params(net):
    nb_params = 0
    for param in net.parameters():
        nb_params += reduce(lambda x, y: x * y, param.shape)
    return nb_params

def padding(x, target_length):

    pad = target_length - x.shape[0]
    return nn.ZeroPad2d(padding=(0, 0, 0, pad))(x)

def truncate_tensor(x, truncate):
    if truncate is not None:
        sequence_length = x.shape[0]
        if sequence_length > truncate:
            keep_idx = sorted(random.sample(range(sequence_length), truncate))
            x = torch.index_select(x, 0, torch.tensor(keep_idx))
    return x

def read_yaml(fpath=None):
    with open(fpath, mode='r') as file:
        yml = yaml.load(file, Loader=yaml.Loader)
        return Dict(yml)

def evaluate(targets, preds):

    evaluation = {
        'ACC' : accuracy_score(targets, preds),
        'BAC' : balanced_accuracy_score(targets, preds),
        'F1' :  f1_score(targets, preds, average='weighted')
    }
    return evaluation

def get_features(dir_features):

    feature_dict = {}

    for file_path in glob.glob(f'{dir_features}/*'):

        slide_name = os.path.splitext(os.path.basename(file_path))[0]

        with h5py.File(file_path, 'r') as f:
            feature_dict[slide_name] = torch.Tensor(f['features'][()])

    return feature_dict

def bar_chart(metric, metric_name, class_names, phase_name):

    df = pd.DataFrame(data={'label' : class_names,
                            metric_name : metric})

    table = wandb.Table(data=df, columns = ['label', metric_name])
    wandb.log({f'{metric_name}_{phase_name}' : wandb.plot.bar(
        table, 'label', metric_name)})

def collate_results(phase_name, step_outputs, class_names):

    n_classes = len(class_names)

    write_flush(f'{phase_name.capitalize()} results:')

    slide_loss = torch.Tensor([x['slide_loss'] for x in step_outputs]).mean()
    wandb.log({f'{phase_name}_loss' : slide_loss})

    probs = torch.cat([x['Y_prob'] for x in step_outputs])
    preds = torch.cat([x['Y_hat'] for x in step_outputs])
    targets = torch.cat([x['label'] for x in step_outputs])

    slide_names = [x['slide_id'][0] for x in step_outputs]

    df = pd.DataFrame(data={'slide_id' : slide_names, 'pred' : preds, 'target' : targets})

    wandb.log({f'preds_{phase_name}' : wandb.Table(dataframe=df)})

    tile_loss = torch.Tensor([x['tile_loss'] for x in step_outputs]).mean()

    probs_tile = torch.cat([x['Y_prob_tile'].squeeze() for x in step_outputs])
    preds_tile = torch.argmax(probs_tile, axis=1)
    targets_tile = torch.cat([x['label_tile'].squeeze() for x in step_outputs])

    if n_classes == 2:
        auc = roc_auc_catch(targets.squeeze(), probs[:, 1])
        f1 = f1_catch(targets.squeeze(), preds)
        #pass
    else:
        auc = roc_auc_catch(targets.squeeze(), probs, multi_class='ovr', average='weighted')
        f1 = f1_catch(targets.squeeze(), preds, average='weighted')
        precision = precision_score(targets.squeeze(), preds, average='weighted')
        recall = recall_score(targets.squeeze(), preds, average='weighted')
        bar_chart(auc, 'auc', class_names, phase_name)
        bar_chart(f1, 'f1', class_names, phase_name)
        bar_chart(precision, 'precision', class_names, phase_name)
        bar_chart(recall, 'recall', class_names, phase_name)

    acc = accuracy_score(targets.squeeze(), preds)
    bac = balanced_accuracy_score(targets.squeeze(), preds)

    wandb.log({f'{phase_name}_acc' : acc})
    wandb.log({f'{phase_name}_bac' : bac})

    wandb.log({f'cm_{phase_name}' : wandb.plot.confusion_matrix(
        preds=list(preds.numpy()),
        y_true=list(targets.numpy()),
        class_names=class_names,
        title=phase_name.capitalize())})

    if n_classes < 3:
        return

    preds_binary = 1. * (preds > 0)
    targets_binary = 1. * (targets > 0)
    probs_binary = torch.stack([probs[:, 0], probs[:, 1:].sum(axis=1)], axis=1)

    target_names = ['Negative', 'Positive']

    auc = roc_auc_catch(targets_binary, probs_binary[:, 1])
    f1 = f1_score(targets_binary, preds_binary)
    acc = accuracy_score(targets_binary, preds_binary)
    bac = balanced_accuracy_score(targets_binary, preds_binary)
    precision = precision_score(targets_binary, preds_binary)
    recall = recall_score(targets_binary, preds_binary)

    wandb.log({f'{phase_name}_auc_binary' : auc})
    wandb.log({f'{phase_name}_f1_binary' : f1})
    wandb.log({f'{phase_name}_acc_binary' : acc})
    wandb.log({f'{phase_name}_bac_binary' : bac})
    wandb.log({f'{phase_name}_precision_binary' : precision})
    wandb.log({f'{phase_name}_recall_binary' : recall})

    wandb.log({f'cm_{phase_name}_binary' : wandb.plot.confusion_matrix(
        preds=preds_binary.numpy(),
        y_true=list(targets_binary.numpy()),
        class_names=target_names,
        title=phase_name.capitalize())})

