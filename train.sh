#!/bin/bash
#SBATCH --job-name=s4-train
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=gpua100
#SBATCH --gres=gpu:1
#SBATCH --mem=180000

# Activate anaconda environment code
source activate $HOME/miniconda3/envs/clam

WANDB__SERVICE_WAIT=300 python train.py --stage=test --config=configs/bcln/bcln.yaml --gpus=1 --folds=5
exit 0

WANDB__SERVICE_WAIT=300 python train.py --stage=test --config=configs/cd8/cd8.yaml --gpus=1 --folds=5
WANDB__SERVICE_WAIT=300 python train.py --stage=test --config=configs/cd8/cd8_topup.yaml --gpus=1 --folds=5
WANDB__SERVICE_WAIT=300 python train.py --stage=test --config=configs/cd8/cd8_all.yaml --gpus=1 --folds=5
exit 0

WANDB__SERVICE_WAIT=300 python train.py --stage=test --config=configs/prostate/panda.yaml --gpus=1 --folds=1
exit 0

WANDB__SERVICE_WAIT=300 python train.py --stage=test --config=configs/prostate/prostate_gleason.yaml --gpus=1 --folds=5
WANDB__SERVICE_WAIT=300 python train.py --stage=test --config=configs/prostate/prostate_binary.yaml --gpus=1 --folds=5
exit 0

