import sys
import numpy as np
import argparse
import random
import pandas as pd

#---->
import wandb
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch import autocast
from torch.cuda.amp import GradScaler
from sklearn.metrics import accuracy_score

#---->
from src.models import S4Former, TileLoss
from src.mil_dataset import MILDataset

from src.utils import *


device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
write_flush(str(device))

#--->Setting parameters
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--stage', default='train', type=str)
    parser.add_argument('--config', default='configs/gr.yaml', type=str)
    parser.add_argument('--gpus', default=[2])
    parser.add_argument('--folds', type=int)
    args = parser.parse_args()
    return args

def forward_pass(model, batch, criterion, cfg):

    lambda_tiles = cfg.Loss.lambda_tiles
    n_accumulate = cfg.General.grad_acc

    #---->inference
    data, label, annotation, slide_id, coords = (batch[0].to(device),
                                                 batch[1].to(device),
                                                 batch[2].to(device),
                                                 batch[3],
                                                 batch[4])

    #---->forward pass
    with autocast(device_type='cuda', dtype=torch.float16):
        results_dict = model(data=data, coords=coords)

    #---->loss (slide)
    logits = results_dict['logits']
    Y_prob = results_dict['Y_prob']
    Y_hat = results_dict['Y_hat']

    logits_tile = results_dict['logits_tile']
    Y_prob_tile = results_dict['Y_prob_tile']
    Y_hat_tile = results_dict['Y_hat_tile']

    #---->loss (tiles)
    logits_tile = logits_tile[:, annotation.squeeze() != -1]
    Y_prob_tile = Y_prob_tile[:, annotation.squeeze() != -1]
    Y_hat_tile = Y_hat_tile[:, annotation.squeeze() != -1]
    annotation = annotation[:, annotation.squeeze() != -1].long()

    with autocast(device_type='cuda', dtype=torch.float16):

        criterion_tile = TileLoss(torch.Tensor(cfg.Loss.tile_weights)).to(device)
        tile_loss = criterion_tile(logits_tile.permute((0, 2, 1)), annotation)

        slide_loss = criterion(logits, label)
        loss = (slide_loss + lambda_tiles * tile_loss) / n_accumulate

    result_dict = {
        'slide_loss': slide_loss.item(),
        'tile_loss' : tile_loss.item(),
        'Y_prob' : Y_prob.detach().cpu(),
        'Y_hat' : Y_hat.detach().cpu(),
        'label' : label.cpu(),
        'Y_prob_tile' : Y_prob_tile.detach().cpu(),
        'Y_hat_tile' : Y_hat_tile.detach().cpu(),
        'label_tile' : annotation.cpu(),
        'slide_id' : slide_id}

    return loss, result_dict


class Checkpoint:

    def __init__(self, mode='loss'):

        self.mode = mode

        if self.mode == 'loss':
            self.best_score = +float('inf')
        else:
            self.best_score = -float('inf')

    def checkpoint(self, result_dict):

        if self.mode == 'loss':
            score = np.array([x['slide_loss'] for x in result_dict]).mean()

            if score < self.best_score:
                self.best_score = score
                return True
        else:
            labels = np.hstack([x['label'] for x in result_dict])
            y_hat = np.hstack([x['Y_hat'] for x in result_dict])
            score = accuracy_score(labels, y_hat)

            if score > self.best_score:
                self.best_score = score
                return True

        return False

def train(model, train_loader, val_loader, criterion, model_path, cfg):

    train_step_outputs, val_step_outputs = [], []

    decay = cfg.Loss.weight_decay
    lr = cfg.Loss.lr

    optimiser = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=decay)
    scaler = GradScaler()

    n_epochs = cfg.General.epochs
    checkpointer = Checkpoint('loss')

    for epoch in range(n_epochs):
        write_flush('#--->Epoch %d' % (epoch + 1))

        model.train()

        #--->Training
        for i, batch in enumerate(train_loader):

            loss, result_dict = forward_pass(model, batch, criterion, cfg)
            train_step_outputs.append(result_dict)
            scaler.scale(loss).backward()

            if (i + 1) % cfg.General.grad_acc == 0 or (i + 1) == len(train_loader):
                scaler.step(optimiser)
                scaler.update()
                optimiser.zero_grad()

        collate_results('train', train_step_outputs, cfg.Data.class_names)
        train_step_outputs.clear()

        #--->Validation
        model.eval()
        with torch.no_grad():

            for batch in val_loader:
                _, result_dict = forward_pass(model, batch, criterion, cfg)
                val_step_outputs.append(result_dict)

        if checkpointer.checkpoint(val_step_outputs):
            write_flush('Checkpoint [val_score=%.04f]' % checkpointer.best_score)
            torch.save(model.state_dict(), model_path)

        collate_results('val', val_step_outputs, cfg.Data.class_names)
        val_step_outputs.clear()

def test(model, test_loader, criterion, model_path, cfg):

    test_step_outputs = []

    model.load_state_dict(torch.load(model_path))
    if not cfg.Model.costs is None:
        model.set_costs(torch.Tensor(cfg.Model.costs).to(device))

    model.eval()
    with torch.no_grad():

        for batch in test_loader:
            _, result_dict = forward_pass(model, batch, criterion, cfg)
            test_step_outputs.append(result_dict)

    collate_results('test', test_step_outputs, cfg.Data.class_names)

def main(args, cfg, fold):

    #---->update
    cfg.Data.fold = fold

    name = cfg.General.name
    fold = cfg.Data.fold
    n_classes = cfg.Data.n_classes
    d_model, d_state = cfg.Model.d_model, cfg.Model.d_state

    run = wandb.init(
        project='s4d',
        name=f'{name}_{args.stage}_{d_model}_{d_state}_fold{fold}')

    train_dataset = MILDataset(cfg.Data, state='train')
    val_dataset = MILDataset(cfg.Data, state='val')
    test_dataset = MILDataset(cfg.Data, state='test')

    batch_size = cfg.Data.train_dataloader.batch_size
    n_workers = cfg.Data.train_dataloader.num_workers

    train_loader = DataLoader(train_dataset, batch_size, shuffle=True)
    val_loader = DataLoader(val_dataset, batch_size, shuffle=False)
    test_loader = DataLoader(test_dataset, batch_size, shuffle=False)

    class_weights = torch.Tensor(cfg.Loss.class_weights).to(device)
    criterion = nn.CrossEntropyLoss(weight=class_weights)

    costs = cfg.Model.costs
    model = S4Former(n_classes, 1024, d_model, d_state, costs).to(device)

    if not cfg.General.pretrained_path is None:
        model.load_state_dict(torch.load(cfg.General.pretrained_path))

    write_flush('#params model: %d' % count_params(model))

    log_path = cfg.General.log_path
    os.makedirs(log_path, exist_ok=True)
    model_path = f'{log_path}/fold{fold}.pt'

    if args.stage == 'train':
        train(model, train_loader, val_loader, criterion, model_path, cfg)

    test(model, test_loader, criterion, model_path, cfg)

    run.finish()

if __name__ == '__main__':

    args = parse_args()
    cfg = read_yaml(args.config)

    write_flush(args)

    for fold in range(args.folds):
        main(args, cfg, fold)

