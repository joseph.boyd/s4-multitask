#!/bin/bash
#SBATCH --job-name=s4-infer
#SBATCH --output=./outputs/%x_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=gpua100
#SBATCH --gres=gpu:1
#SBATCH --mem=180000

# Activate anaconda environment code
source activate $HOME/miniconda3/envs/clam

python infer.py --config=configs/bcln/bcln.yaml \
                --annot_dir=/workdir/shared/cpm4c/GR_BCLN/tmp_backup/ANNOTATIONS \
                --wsi_dir=/workdir/shared/cpm4c/WSI_DATA/GR_DATA \
                --folds=5
